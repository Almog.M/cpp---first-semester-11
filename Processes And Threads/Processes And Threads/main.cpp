#include "threads.h"



int main()
{
	vector<int> primes1;
	vector<int> primes3;
	vector<int> thirdParagraphPrimes;

	//First paragraph...
	std::cout << "The first paragraph: ";
	call_I_Love_Threads();

	cout << "Second paragraph:\n\n";
	//Second paragraph...
	try 
	{
		getPrimes(0,90, primes1);
	}
	catch (exception& e)
	{
		std::cout << e.what() << "\n";
	}

	cout << "Seconds paragraph: first primes vector:\n";
	printVector(primes1);
	

	primes3 = callGetPrimes(0, 289);

	cout << "Second paragraph: third primes vector:\n";
	printVector(primes3);

	cout << "Third paragraph:\n\n";
	cout << "Third paragraph: prints first vector:\n";
	
	thirdParagraphPrimes = callGetPrimes(0, 1000);
	//printVector(thirdParagraphPrimes);
	
	cout << "Third paragraph: print second vector:\n";
	
	thirdParagraphPrimes = callGetPrimes(0, 100000);
	//printVector(thirdParagraphPrimes);
	
	cout << "Third paragraph: print third vector:\n";
	
	thirdParagraphPrimes = callGetPrimes(0, 1000000);
	//printVector(thirdParagraphPrimes);

	//Fourth paragraph...
	cout << "Fourth paragraph:\n";
	callWritePrimesMultipleThreads(1, 100, "primes2.txt", 10);

	//Fifths paragraph...
	cout << "Fifth paragraph: \n";
	callWritePrimesMultipleThreads(1, 10000, "primes2.txt", 10);
	cout << "Check the primes2 file....\n";
	system("pause");

	callWritePrimesMultipleThreads(1, 100000, "primes2.txt", 10);
	cout << "Check the primes2 file....\n";
	system("pause");

	callWritePrimesMultipleThreads(1, 1000000, "primes2.txt", 10);
	cout << "Check the primes2 file....\n";
	system("pause");

	return 0;
}